### Keybase proof

I hereby claim:

  * I am slachiewicz on github.
  * I am slachiewicz (https://keybase.io/slachiewicz) on keybase.
  * I have a public key ASBl8NimUbKIctTlePSoE9y-Hv4LQYDqP6ccLp7wpiZ9Xwo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "012065f0d8a651b28872d4e578f4a813dcbe1efe0b4180ea3fa71c2e9ef0a6267d5f0a",
      "host": "keybase.io",
      "kid": "012065f0d8a651b28872d4e578f4a813dcbe1efe0b4180ea3fa71c2e9ef0a6267d5f0a",
      "uid": "bb655b786140d29600cf61064927c419",
      "username": "slachiewicz"
    },
    "merkle_root": {
      "ctime": 1589029047,
      "hash": "70f30397ec39cbdfa89f880b4deb13803ced3b782de828530979442af6e30096f30da13d31fb8aabf1fa8ef2ccfcb099142714a515dcad4670643a0a7c496de9",
      "hash_meta": "26528197ca9af9f65726b4a1723f3fdf737000934cba3deccf0b09e116b6c3b6",
      "seqno": 16230089
    },
    "service": {
      "entropy": "osh9aHLNFZ9niwLCnHfgRPoj",
      "name": "github",
      "username": "slachiewicz"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.4.2"
  },
  "ctime": 1589029068,
  "expire_in": 504576000,
  "prev": "b311a2274067749822cd84d893152edec2382daee5f228aef8a2ff378382c69e",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASBl8NimUbKIctTlePSoE9y-Hv4LQYDqP6ccLp7wpiZ9Xwo](https://keybase.io/slachiewicz), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgZfDYplGyiHLU5Xj0qBPcvh7+C0GA6j+nHC6e8KYmfV8Kp3BheWxvYWTESpcCBMQgsxGiJ0BndJgizYTYkxUu3sI4La7l8iiu+KL/N4OCxp7EIMpRqLa1o4DAJRAb0lD2as0Gh0bjuwGnTuK+n+7rjDnXAgHCo3NpZ8RAMTh3OJXqzGj0/8KFs56I7oRmbPm4Q9nhRwcRppugjwS7/7RBE/aIeogcyajLjeDmQ2+GVofiL4VyxrK0LG1BCahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIOnfbg6uaFXubcNEP8gGH0HbD0UBHQwX53XeUnRlI4HZo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/slachiewicz

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id slachiewicz
```
